/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
/*Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0.
 Si es 0, cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa
 Para la resolución de la lógica, siga el diagrama de flujo siguiente:*/

#include <stdio.h>
#include "main.h"

/*==================[macros and definitions]=================================*/
const int cte = 1234;

/*==================[internal functions declaration]=========================*/

int main(void)
{

		uint32_t aux= cte;
		uint16_t A;
	    uint32_t mascara=0x00000000;

	    mascara=(1<<4);

	    if((aux&=mascara)==0)
	    {A=0;
	    printf("%d aux\r\n",aux);
	    printf("%d A\r\n",A);
	    }

	    else
	    {A=0xAA;
	    printf("%d aux\r\n",aux);
	    printf("%d A\r\n",A);}
	return 0;
}

/*==================[end of file]============================================*/

