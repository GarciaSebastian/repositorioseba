/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
/* C) Escriba una función que reciba un dato de 32 bits, la cantidad de dígitos de salida y un puntero a
un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD,
guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.*/


#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int8_t Binario_A_Bcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ){

	uint32_t resto;
	int i;

	for(i=digits; i>0; i--){
		resto=data%10;				/*Me devuelve el resto de la division por 10*/
		bcd_number[i-1]=resto;		/*Cargo el resto de la division en un arreglo*/
		data=data/10;				/*Divido en 10 el dato ingresado*/
	}
	}



int main(void)
{
	uint8_t digito[3];

	Binario_A_Bcd(321, 3, digito);
	printf("%d digito 1\r\n", digito[0]);
	printf("%d digito 2\r\n", digito[1]);
	printf("%d digito 3\r\n", digito[2]);

	return 0;
}

/*==================[end of file]============================================*/

