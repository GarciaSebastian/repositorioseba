/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
//Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
//caracteres y edad.
//a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
//b. Defina un puntero a esa estructura y cargue los campos con los datos de su
//compañero (usando acceso por punteros).

#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>


/*==================[macros and definitions]=================================*/

struct clase {
		char nombre[12];
		char apellido[20];
		int edad;
};
/*==================[internal functions declaration]=========================*/

int main(void)
{
	struct clase alumno1;
	char nom1[]="Sebastian";
	char ape1[]="Garcia";

	strcpy(alumno1.nombre,nom1);
	strcpy(alumno1.apellido,ape1);
	alumno1.edad=18;


	printf("=========================================================================================================== \r\n");
	printf("NOMBRE=%s.\n",alumno1.nombre);
	printf("APELLIDO=%s.\n", alumno1.apellido);
	printf("EDAD=%d.\n", alumno1.edad);

	printf("=========================================================================================================== \r\n");

	struct clase *alumno2;
	char nom2[]="Pampita";
	char ape2[]="Ardohain";

	strcpy(alumno2->nombre,nom2);
	strcpy(alumno2->apellido,ape2);
	alumno2->edad=30;

	printf("=========================================================================================================== \r\n");
		printf("NOMBRE=%s.\n", alumno2->nombre);
		printf("APELLIDO=%s.\n", alumno2->apellido);
		printf("EDAD=%d.\n", alumno2->edad);

		printf("=========================================================================================================== \r\n");



	return 0;
}

/*==================[end of file]============================================*/

