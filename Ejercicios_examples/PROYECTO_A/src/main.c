/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/
typedef struct {
    	uint8_t n_led;		/*Numero de led a controlar*/
    	uint8_t n_ciclos;	/*Cantidad de ciclos ENCENDIDO/APAGADO*/
    	uint8_t tiempo;		/*Tiempo del ciclo*/
    	uint8_t modo;		/*ON OFF TOGGLE*/
    } my_leds;


    const int ON=1;		/*ENCENDIDO*/
    const int OFF=0;	/*APAGADO*/
    const int TOGGLE=7;	/*INTERMITENTE*/

    uint8_t i=0;
    uint8_t j=0;
/*==================[internal functions declaration]=========================*/
void control(my_leds *led){  /*paso un puntero como argumento*/

	uint8_t i=0;
	uint8_t j=0;

	if(led->modo==ON){
		switch(led->n_led) {

					case 1:  printf("ENCIENDE LED 1 \r\n");	break;
					case 2:  printf("ENCIENDE LED 2 \r\n");	break;
					case 3:  printf("ENCIENDE LED 3 \r\n");	break;
					default:  printf("NO ENCIENDE NINGUN LED \r\n");}
			}

		else{   if(led->modo==OFF){

			switch(led->n_led){

					case 1:  printf("APAGA LED 1 \r\n");	break;
					case 2:  printf("APAGA LED 2 \r\n");	break;
					case 3:  printf("APAGA LED 3 \r\n");	break;
					default:  printf("NO APAGA NINGUN LED \r\n");
													   }

									    }



	else {if(led->modo==TOGGLE){
									while(i<led->n_ciclos){

												switch(led->n_led) {
												case 1: printf(" TOGGLE LED 1 \r\n");	break;
												case 2: printf(" TOGGLE LED 2 \r\n");	break;
												case 3: printf(" TOGGLE LED 3 \r\n");	break;
													i++;	}

													while(j<led->tiempo){

													j++;}

													  }

				} else{printf(" FIN \r\n");}

		}

		}
}

int main(void)
{my_leds LED;
LED.modo=1;
LED.n_ciclos=5;
LED.n_led=1;
LED.tiempo=10;
control(&LED);

	return 0;
}

/*==================[end of file]============================================*/

