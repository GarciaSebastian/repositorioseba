/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
//a. Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare
//cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
//cargue cada uno de los bytes de la variable de 32 bits.
//b. Realice el mismo ejercicio, utilizando la definición de una “union”.

#include "main.h"
#include <stdio.h>
#include "stdint.h"
/*==================[macros and definitions]=================================*/
uint32_t variable=0x01020304;
uint8_t variable1,variable2,variable3,variable4;



/*==================[internal functions declaration]=========================*/

int main(void)
{
   uint32_t masc1=0x000000FF;
   variable1=variable&masc1;
   printf("%d variable1 \r\n", variable1);

   uint32_t masc2=0x0000FF00;
   variable2=(variable&masc2)>>8;
   printf("%d variable2 \r\n", variable2);

   uint32_t masc3=0x00FF0000;
   variable3=(variable&masc3)>>16;
   printf("%d variable3 \r\n", variable3);

   uint32_t masc4=0xFF000000;
   variable4=(variable&masc4)>>24;
   printf("%d variable4 \r\n", variable4);

   printf("=========================================================================================================== \r\n");

   union var32 {

   	struct cada_byte {

   		uint8_t byte1;
   		uint8_t byte2;
   		uint8_t byte3;
   		uint8_t byte4;
   	};
   	uint32_t sumbyt;
   };

   struct cada_byte s;
   union var32 b;
   b.sumbyt=0x01020304;

   printf("%x byte \r\n",b.sumbyt);
   printf("%x byte4 \r\n",s.byte4);
   printf("%x byte3 \r\n",s.byte3);
   //printf("%x byte3 \r\n",s.byte3);
   //printf("%x byte4 \r\n",s.byte4);


	return 0;
}

/*==================[end of file]============================================*/

