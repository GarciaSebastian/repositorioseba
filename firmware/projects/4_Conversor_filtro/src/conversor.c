/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/conversor.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "Delay.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
uint16_t VALOR;
uint16_t contador;
bool Bandera=false;
uint16_t salida_filtrada;
uint16_t salida_sin_filtrar;
float alfa=0;
bool Bandera_Filtrar;
float dt=0.002;  							/*diferencial tiempo*/
uint16_t Frecuencia_Corte=0; 				/*frecuencia de corte*/
float RC=0;									/*Constante de tiempo*/



uint8_t TABLA[] = {
17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
17,17,17

} ;


/*==================[internal functions declaration]=========================*/

void comenzar_conversion(void);
void conversion(void);
void puerto_serie(void);


/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_B,2,&comenzar_conversion}; 					/*Estructura del timer*/
serial_config puerto={SERIAL_PORT_PC,115200,NULL};							/*Estructura comunicacion serie (uart)*/
analog_input_config AD_conversor={CH1,AINPUTS_SINGLE_READ,&conversion};		/*Estructura Connversor AD*/

/*==================[external functions definition]==========================*/

void comenzar_conversion(void){						/*Con esta funcion el timer comienza la conversion cada 2mseg*/

	AnalogStartConvertion();
	 if(Bandera==true){

		if(contador<256){							/*si contador es menor a 256 que es el tamaño del vector ecg que lo acumule en contador*/
			AnalogOutputWrite(TABLA[contador]);
	 		 		contador++;
	 		 			  }

	 	if(contador>=256){							/*Si contador es mayor que 256 contador se pone en 0*/
	 		  contador=0;
	 		 			 	}
	 		 			 	}
	 	Bandera=!Bandera;							/*Cambio de bandera*/

}

void filtrar(void){

	Bandera_Filtrar=1;								/*Interrupcion para comenzar a filtrar*/

}

void no_filtrar(void){

	Bandera_Filtrar=0;								/*Interrupcion para dejar de filtrar*/
}

void aumenta_frecuencia(void){

	Frecuencia_Corte++;								/*Incremento frecuencia de corte*/

	}

void disminuye_frecuencia(void){
	Frecuencia_Corte--;								/*decremento frecuencia de corte*/
}

void conversion(void){								/*Lee lo que ingresa por CH1 y lo pasa a Valor*/


	AnalogInputRead(CH1,&VALOR);					/*Leo lo que entra en CH1 y se lo paso a la variable valor*/



	if(Bandera_Filtrar==1){

		RC=1/(Frecuencia_Corte*2*3.14);
		alfa=dt/(dt+RC);

	salida_sin_filtrar=salida_filtrada;
	salida_filtrada=salida_sin_filtrar+alfa*(VALOR-salida_sin_filtrar);
	puerto_serie();
	}

}



void puerto_serie(){													/*Aqui convierto un string en decimal y lo envio por puerto serie*/

		UartSendString(SERIAL_PORT_PC,"#");
		UartSendString(SERIAL_PORT_PC, UartItoa(VALOR,10)); 			/*señal sin filtrar*/
		UartSendString(SERIAL_PORT_PC,",");
		UartSendString(SERIAL_PORT_PC, UartItoa(salida_filtrada,10));	/*señal filtrada*/
		UartSendString(SERIAL_PORT_PC,"\r");							/*caracter de fin de linea*/


}

int main(void)
{
	SystemClockInit();

		LedsInit();							/*inicializo leds*/
		TimerInit(&my_timer);				/*Inicializo timer*/
		TimerStart(TIMER_B);

		UartInit(&puerto);					/*Inicializo uart*/

		AnalogInputInit(&AD_conversor);		/*Inicializo funcion de entrada*/
		AnalogOutputInit();					/*Inicializo funcion de salida*/

		SwitchesInit();
		SwitchActivInt(SWITCH_1, filtrar);
		SwitchActivInt(SWITCH_2, no_filtrar);
		SwitchActivInt(SWITCH_3, disminuye_frecuencia);
		SwitchActivInt(SWITCH_4, aumenta_frecuencia);


   
    while(1)
    {

	}
    

}

/*==================[end of file]============================================*/

