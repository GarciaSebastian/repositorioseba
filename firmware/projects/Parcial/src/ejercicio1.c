/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ejercicio1.h"       /* <= own header */

#include "hc_sr4.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"	/*Driver timer*/


/*==================[macros and definitions]=================================*/
uint16_t Distancia_en_centimetros=0;



/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

void timer_distancia_menor(void);  /*Declaro las funciones a utilizar*/
void timer_distancia_mayor(void);

/*==================[external data definition]===============================*/

timer_config my_timerA = {TIMER_B,1,&timer_dstancia_menor};   /*Estrucutura del timer B */
timer_config my_timerB = {TIMER_A,5,&timer_dstancia_mayor};	/*Estrucutura del timer A */

/*==================[external functions definition]==========================*/
void timer_distancia_menor(void){

	Distancia_en_centimetros=HcSr04ReadDistanceCentimeters();

	if((Distancia_en_centimetros>1)&&(Distancia_en_centimetros<10)){	/*si la distancia es mayor a uno y menor a 10 el led va a parpadear a 1mseg */
		LedsOffAll();
		LedOn(LED_1);
	}
}

void timer_distancia_mayor(void){
	Distancia_en_centimetros=HcSr04ReadDistanceCentimeters();			/*si la distancia es mayor a 10 y menor a 20 el led va a parpadear a 2mseg */

	if((Distancia_en_centimetros>10)&&(Distancia_en_centimetros<20)){
		LedsOffAll();
		LedOn(LED_1);
	}
}

int main(void)
{
	SystemClockInit();

	LedsInit();						/*inicializo leds*/
	HcSr04Init(T_FIL2,T_FIL3);
	TimerInit(&my_timerA);			/*Inicializo timer A*/
	TimerStart(TIMER_A);			/*Comienza timer A*/

	TimerInit(&my_timerB);			/*Inicializo timer B*/
	TimerStart(TIMER_B);			/*Comienza timer B*/

	SwitchesInit();
}

/*==================[end of file]============================================*/

