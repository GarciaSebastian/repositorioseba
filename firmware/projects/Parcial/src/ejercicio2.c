/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Parcial/inc/ejercicio2.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "Delay.h"

#define ON 1
#define OFF 0
/*==================[macros and definitions]=================================*/


void comenzar_conversion(void);
void conversion(void);

/*==================[internal data definition]===============================*/

timer_config my_timer = {TIMER_B,20,&comenzar_conversion}; 					/*Estructura del timer a 20mseg*/
serial_config puerto={SERIAL_PORT_PC,115200,NULL};							/*Estructura comunicacion serie (uart)*/
analog_input_config AD_conversor={CH2,AINPUTS_SINGLE_READ,&conversion};		/*Estructura Connversor AD*/

bool fin_conversion;	/*BANDERA*/
uint16_t CANT_MUESTRAS=20;
uint16_t  VOLTAGES[50]; /* VECTOR QUE GUARDAD LOS VOLTTAGES CADA 20 ms*/
float PROMEDIO=0;
uint16_t AUX=0;
uint8_t b=0;
uint16_t VALOR;
/*==================[internal functions declaration]=========================*/




/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
float PROM(void)  /* FUNCION QUE ENVIA EL PROMEDIO DE LOS VOLTAGES A TRAVES DE LA VARIABLE AUX*/
{
	int a;
	AUX=0;
	for(a=0;a<CANT_MUESTRAS;a++)
	{
		AUX=AUX+TEMPERATURAS[a];
	}
	AUX=AUX/CANT_MUESTRAS;
	return(AUX);
}



void comenzar_conversion(void){		/*Con este funcion el timer comienza la conversion cada 20mseg*/

	AnalogStartConvertion();
	fin_conversion=ON;
}

void conversion(void){	/* Aqui convierto lo que ingresa por CH2 y se lo paso a valor, luego calculo el promedio*/
	if(fin_conversion==ON)
		{

		AnalogInputRead(CH2, &VALOR);
		VOLTAGES[b]=VALOR;
		b++;
		if(b==CANT_MUESTRAS)
			{
			b=0;
			PROMEDIO=PROM();

			}
		fin_conversion=OFF;
		}
}

void enviar_por_puerto(){		/*Envio a travez del puerto y convierto el caracter a decimal*/

	if(fin_conversion=ON){
				conversion();
			}
			UartSendString(SERIAL_PORT_PC, UartItoa(PROMEDIO,10));
			UartSendString(SERIAL_PORT_PC,",");
			UartSendString(SERIAL_PORT_PC,"\r");
}

int main(void)
{
			SystemClockInit();

			LedsInit();							/*inicializo leds*/
			TimerInit(&my_timer);				/*Inicializo timer*/
			TimerStart(TIMER_B);

			UartInit(&puerto);					/*Inicializo uart*/

			AnalogInputInit(&AD_conversor);		/*Inicializo funcion de entrada*/
			AnalogOutputInit();
}

/*==================[end of file]============================================*/

