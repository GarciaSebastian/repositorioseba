﻿Ejercicio 1:
Diseñar e implementar el firmware de un sistema de medición de proximidad.
Empleando el sensor HC-SR04 el sistema debe aumentar la frecuencia de parpadeo del LED 1 cuando se aproxima a un objeto.
Defina un intervalo de parpadeos que indiquen proximidad en un rango de entre 1 cm y 20 cm. El sistema debe aumentar la frecuencia de parpadeo a medida que el objeto se aproxima (y disminuir cuando se aleja). 
Se deben usar interrupciones y timer.

Para este problema utilizo dos timer para el parpadeo de los leds. Si bien se podria hacer la aplicacion con un solo timer, para
no gastarlos tan tempranamente. Yo utilizo dos timer porque el problema es sencillo y despues no tenia mas requerimentos.


Ejercicio 2:
Diseñe e implemente un firmware que mida el voltaje generado por el punto medio de un potenciómetro conectado a la entrada CH2 de la EDU-CIAA 
a una frecuencia de muestreo de 50 Hz y envíe el valor promedio del último segundo por el puerto serie a la PC en un formato de caracteres legible por consola (uno por renglón). Se debe usar interrupciones y timer. (Conecte los extremos del potenciómetro a VDDA y GNDA).

Para este problema ingresa la señal por ch2, la cantidad de muesra es 20 y el perido es 20mseg. Se convierte la señal analogica, se calcula el promedio y se lo manda por puerto serie.