/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
/*2.	Modifique la aplicación 1_blinking_switch de manera de hacer titilar los leds verde,
 		amarillo y rojo al mantener presionada las teclas 2, 3 y 4 correspondientemente.
 	 	También se debe poder hacer titilar los leds individuales del led RGB, para ello se deberá
 	 	mantener presionada la tecla 1 junto con la tecla 2, 3 o 4 correspondientemente.*/

#include "blinking_switch.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define COUNT_DELAY 3000000
/*==================[macros and definitions]=================================*/
void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

void parpadea_amrarillo(void){
	LedOn(LED_2);  /* llamo a la funcion LedToggle y prendo el led 1 (Amarillo)*/
}

void parpadea_rojo(void){
	LedOn(LED_3);  /* llamo a la funcion LedToggle y prendo el led 2 (Rojo)*/
}

void parpadea_verde(void){
	LedOn(LED_1);  /* llamo a la funcion LedToggle y prendo el led 3 (Verde)*/
}

void apaga_amarillo(void){
LedOff(LED_2);			/* llamo a la funcion LedToggle y apago el led 1 (Amarillo)*/
}

void apaga_rojo(void){
LedOff(LED_3);			/* llamo a la funcion LedToggle y apago el led 2 (Rojo)*/
}

void apaga_verde(void){
LedOff(LED_1);			/* llamo a la funcion LedToggle y apago el led 3 (Verde)*/
}

void ON_RGB_R(void){
	LedOn(LED_RGB_R); 	/* llamo a la funcion LedOn y prendo el led Rojo*/
}

void ON_RGB_G(void){
	LedOn(LED_RGB_G); 	/* llamo a la funcion LedOn y prendo el led Verde*/
}

void ON_RGB_B(void){
	LedOn(LED_RGB_B); 	/* llamo a la funcion LedOn y prendo el led Azul*/
}

void OFF_RGB_R(void){
	LedOff(LED_RGB_R); 	/* llamo a la funcion LedOff y apago el led Rojo*/
}

void OFF_RGB_G(void){
	LedOff(LED_RGB_G); 	/* llamo a la funcion LedOff y apago el led Verde*/
}

void OFF_RGB_B(void){
	LedOff(LED_RGB_B); 	/* llamo a la funcion LedOff y apago el led Azul*/
}
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
    while(1)
    {
    	teclas  = SwitchesRead();
    	switch(teclas)
    	{
    	case SWITCH_1|SWITCH_2:

    	    				ON_RGB_R();
    	    				Delay();
    	    				OFF_RGB_R();
    	    				Delay();
    	    				break;

    	case SWITCH_1|SWITCH_3:

    	    				ON_RGB_G();
    	    				Delay();
    	    				OFF_RGB_G();
    	    				Delay();
    	    				break;

    	case SWITCH_1|SWITCH_4:

    	    				ON_RGB_B();
    	    				Delay();
    	    				OFF_RGB_B();
    	    				Delay();
    	    				break;

    	case SWITCH_2:
    			parpadea_verde();
    			Delay();
    			apaga_verde();
    			Delay();
    		break;

    	case SWITCH_3:
    			parpadea_amrarillo();
    			Delay();
    			apaga_amarillo();
    			Delay();
    		break;

    		case SWITCH_4:
    			parpadea_rojo();
    			Delay();
    			apaga_rojo();
    			Delay();
    		 break;
    	}
	}


    	}
    


/*==================[end of file]============================================*/



