/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../3_pruebahc_sr4/inc/pruebasensor.h"       /* <= own header */

#include "hc_sr4.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"	/*Driver retardo*/
#include "timer.h"	/*Driver timer*/
#include "uart.h"	/*Driver puerto serie*/


/*==================[macros and definitions]=================================*/
#define DECIMAL 10

uint16_t Distancia_en_centimetros=0;
uint16_t Distancia_en_pulgadas=0;
uint16_t ENCENDIDO=0; /* Bandera*/
uint16_t HOLD=0; /*Bandera*/
bool Valor=false;
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

void timer_distancia(void);
void recibir_dato(void);
/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_B,2000,&timer_distancia};
serial_config puerto={SERIAL_PORT_PC,115200,&recibir_dato};

/*==================[external functions definition]==========================*/
void recibir_dato(void){				/*Funcion para recibir datos de una terminal externa a la placa*/
uint8_t byte;

	UartReadByte(SERIAL_PORT_PC, &byte);	/*Recibe un dato tipo string 'O' prende y apaga la placa - 'H' Congela el ultimo dato medido*/

	if(byte=='O'){ENCENDIDO=!ENCENDIDO;}

	if(byte=='H'){HOLD=!HOLD;}
	}

void TECLA_1(void){		/*Funcion para cambiar estado de tecla 1*/
	ENCENDIDO=!ENCENDIDO;
}

void TECLA_2(void){		/*Funcion para cambiar estado de tecla 2*/
	HOLD=!HOLD;
}

void timer_distancia(void){
	Valor=true;

	if((ENCENDIDO==1)&&(HOLD==0)){
	Distancia_en_centimetros=HcSr04ReadDistanceCentimeters();

	UartSendString(SERIAL_PORT_PC, "Distancia Medida: "); 						/*Envio a la terminal un string ("Distancia Medida")*/
	UartSendString(SERIAL_PORT_PC, UartItoa(Distancia_en_centimetros,DECIMAL)); /*Convierto el valor medido por el sensor en decimal y lo mando a la termminal*/
	UartSendString(SERIAL_PORT_PC,"cm \r\n");} 									/*Envio un string ("cm") a la terminal y le digo que al proximo dato me lo muestre en el proximo renglon*/
}

int main(void)
{

	SystemClockInit();
	LedsInit();
	HcSr04Init(T_FIL2,T_FIL3);
	TimerInit(&my_timer);
	TimerStart(TIMER_B);

	UartInit(&puerto);

	SwitchesInit();
	SwitchActivInt(SWITCH_1, TECLA_1);	/*Inicializo las funciones*/
	SwitchActivInt(SWITCH_2, TECLA_2);

	while(1)
    {		if(Valor==true){
    		Valor=false;

			if((ENCENDIDO==1)&&(HOLD==0)){


			if(Distancia_en_centimetros<10){
					LedsOffAll();
					LedOn(LED_RGB_B);

			}

			if((Distancia_en_centimetros>10)&&(Distancia_en_centimetros<20)){
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);

			}

			if((Distancia_en_centimetros>20)&&(Distancia_en_centimetros<30)){
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);

			}

			if(Distancia_en_centimetros>30){
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOn(LED_3);

			}}


			if(ENCENDIDO==0){
				HOLD=0;
				LedsOffAll();}}
}}


/*==================[end of file]============================================*/


