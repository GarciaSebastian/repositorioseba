/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../3_pruebahc_sr4/inc/pruebasensor.h"       /* <= own header */

#include "hc_sr4.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"

uint16_t Distancia_en_centimetros=0;
uint16_t Distancia_en_pulgadas=0;
uint16_t TECLA1=0; /* Bandera*/
uint16_t HOLD=0; /*Badera*/

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void TECLA_1(void){		/*Funcion para cambiar estado de tecla 1*/
	TECLA1=!TECLA1;
}

void TECLA_2(void){		/*Funcion para cambiar estado de tecla 2*/
	HOLD=!HOLD;
}

int main(void)
{
	SystemClockInit();
	LedsInit();
	HcSr04Init(T_FIL2,T_FIL3);

	SwitchesInit();						/*Llamo a la funcion*/
	SwitchActivInt(SWITCH_1, TECLA_1);	/*Inicializo las funciones*/
	SwitchActivInt(SWITCH_2, TECLA_2);

	while(1)
    {
			if((TECLA1==1)&&(HOLD==0)){
			Distancia_en_centimetros=HcSr04ReadDistanceCentimeters();

			if(Distancia_en_centimetros<10){
					LedsOffAll();
					LedOn(LED_RGB_B);
					DelayMs(500);
			}

			if((Distancia_en_centimetros>10)&&(Distancia_en_centimetros<20)){
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					DelayMs(500);
			}

			if((Distancia_en_centimetros>20)&&(Distancia_en_centimetros<30)){
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					DelayMs(500);
			}

			if(Distancia_en_centimetros>30){
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOn(LED_3);
					DelayMs(500);
			}}


			if(TECLA1==0){			/*Apago todas las luces cuando tecla1=0*/
				HOLD=0;
				LedsOffAll();}
			}

}


/*==================[end of file]============================================*/

