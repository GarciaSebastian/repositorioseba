/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/hc_sr4.h"       /* <= own header */
#include "systemclock.h"
#include <delay.h>

#include <bool.h>
#include <stdint.h>

gpio_t ECHO,TRIGGER;

const int time_echo=1;
const int time_trigger=10;

/*==================[macros and definitions]=================================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger) {

	/*inicializo los puertos definidos en un struct de gpio.h*/
	ECHO=echo;
	TRIGGER=trigger;
	GPIOInit(ECHO, GPIO_INPUT);
	GPIOInit(TRIGGER, GPIO_OUTPUT);
	GPIOOff(TRIGGER);
	return true;
	}

int16_t HcSr04ReadDistanceCentimeters(void){

	uint16_t contador_centimetros=0;

		GPIOOn(TRIGGER);
		DelayUs(time_trigger);
		GPIOOff(TRIGGER);
				while(!(GPIORead(ECHO))){

					}

				while((GPIORead(ECHO)))
						{
							DelayUs(time_echo);
							contador_centimetros++;
						}
						return ((contador_centimetros/58));


}

int16_t HcSr04ReadDistanceInches(void)
{
	uint16_t contador_pulgadas=0;

	GPIOOn(TRIGGER); /*Prendo el trigger con un delay de 10 micro segundos*/
	DelayUs(time_trigger);
	GPIOOff(TRIGGER); /*Apago el trigger*/
		while(!(GPIORead(ECHO)))
		{

		}

			while((GPIORead(ECHO)))
					{
					DelayUs(time_echo);
					contador_pulgadas++;
	}
					return (contador_pulgadas/(148));

}

bool HcSr04Deinit(){

	return false;
}




